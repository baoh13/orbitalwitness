﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrbitalWitness.Challenge
{
    public class RootObject
    {
        public LeaseSchedule LeaseSchedule { get; set; }
    }

    public class LeaseSchedule
    {
        public string ScheduleType { get; set; }
        public List<ScheduleEntry> ScheduleEntry { get; set; }
    }

    public class ScheduleEntry
    {
        public string EntryNumber { get; set; }
        public string EntryDate { get; set; }
        public string EntryType { get; set; }
        public List<string> EntryText { get; set; }
    }
}
