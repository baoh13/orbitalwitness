﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrbitalWitness.Challenge
{
    public interface IParser
    {
        Entry Parse(List<string> entryTexts);
    }
}
