﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace OrbitalWitness.Challenge
{
    class Program
    {
        static readonly string DATA_FILE = "schedule_of_notices_of_lease_examples.json";

        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello Orbital Witness!");
            IParser parser = new Parser();

            var leases = (await LoadData()).Select(d => d.LeaseSchedule).ToList();

            leases.ForEach(lease =>
            {
                lease.ScheduleEntry.ForEach(e =>
                {
                    var entry = parser.Parse(e.EntryText);

                    Console.WriteLine("###########Entry#############");
                    Console.WriteLine(entry.RegistrationDatePlan);
                    Console.WriteLine(entry.PropertyDescription);
                    Console.WriteLine(entry.LeaseDate);
                    Console.WriteLine(entry.LeaseTitle);
                });
            });
        }

        private static async Task<List<RootObject>> LoadData()
        {
            using (StreamReader reader = new StreamReader(DATA_FILE))
            {
                string text = await reader.ReadToEndAsync();

                var leases = JsonConvert.DeserializeObject<List<RootObject>>(text);

                return leases;
            }
        }
    }
}
