﻿namespace OrbitalWitness.Challenge
{
    public class Indexes
    {
        public int RegDateIndex { get; set; } = -1;
        public int PropDesIndex { get; set; } = -1;
        public int LeaseDateIndex { get; set; } = -1;
        public int LeaseTitleIndex { get; set; } = -1;
        public bool HasRegDate { get { return RegDateIndex != -1; } }
        public bool HasPropertyDescription { get { return PropDesIndex != -1; } }
        public bool HasLeaseDate { get { return LeaseDateIndex != -1; } }
        public bool HasLeaseTitle { get { return LeaseTitleIndex != -1; } }
    }
}
