﻿using System.Collections.Generic;

namespace OrbitalWitness.Challenge
{
    public class Entry
    {
        public string RegistrationDatePlan { get; set; }
        public string PropertyDescription { get; set; }
        public string LeaseDate { get; set; }
        public string LeaseTitle { get; set; }
        public List<string> Notes { get; set; }
    }
}
