﻿using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;

namespace OrbitalWitness.Challenge
{
    public class Parser : IParser
    {
        public Entry Parse(List<string> entryTexts)
        {
            var entry = new Entry();


            if (entryTexts != null && entryTexts.Any())
            {             
                Indexes indexes = null;
                var notes = new List<string>();
                var regDate = "";
                var propDescription = "";
                var leaseDate = "";
                var leaseTitle = "";

                for (int i = 0; i < entryTexts.Count(); i++)
                {
                    var line = entryTexts[i];
                    if (!string.IsNullOrWhiteSpace(line))
                    {
                        var length = line.Length;

                        if (line.StartsWith("NOTE"))
                        {
                            notes.Add(line);
                        }
                        else if (i == 0)
                        {
                            //Get indexes
                            indexes = GetIndexes(entryTexts[i]);

                            if (!indexes.HasRegDate)
                            {
                                if (entryTexts.Count() == 1)
                                {
                                    notes.Add(line); // Only one line e.g: "ITEM CANCELLED on 10 June 2015."
                                    continue;
                                }
                            }

                            regDate += indexes.HasPropertyDescription ? " " + GetText(line, indexes.RegDateIndex, indexes.PropDesIndex) : "";
                            propDescription += indexes.HasPropertyDescription ? " " + GetText(line, indexes.PropDesIndex, indexes.HasLeaseDate ? indexes.LeaseDateIndex : length) : "";
                            leaseDate += indexes.HasLeaseDate ? " " + GetText(line, indexes.LeaseDateIndex, indexes.HasLeaseTitle ? indexes.LeaseTitleIndex : length) : "";
                            leaseTitle += indexes.HasLeaseTitle ? " " + GetText(line, indexes.LeaseTitleIndex, length) : "";
                        }
                        else
                        {
                            if (!indexes.HasRegDate)
                            {
                                propDescription += indexes.HasPropertyDescription ? " " + GetText(line, indexes.PropDesIndex, indexes.HasLeaseDate ? indexes.LeaseDateIndex : length) : "";
                                leaseDate += indexes.HasLeaseDate ? " " + GetText(line, indexes.LeaseDateIndex, indexes.HasLeaseTitle ? indexes.LeaseTitleIndex : length) : "";
                                leaseTitle += indexes.HasLeaseTitle ? " " + GetText(line, indexes.LeaseTitleIndex, length) : "";
                                continue;
                            }

                            else
                            {
                                if (length <= indexes.PropDesIndex)
                                {
                                    // only regDate
                                    regDate += " " + line.Substring(0, length);
                                }
                                else if (length > indexes.PropDesIndex && length <= indexes.LeaseDateIndex)
                                {
                                    // regDate, propDesc
                                    regDate += " " + GetText(line, indexes.RegDateIndex, indexes.PropDesIndex);
                                    propDescription += " " + GetText(line, indexes.PropDesIndex, length);
                                }
                                else if (length > indexes.LeaseDateIndex && length <= indexes.LeaseTitleIndex)
                                {
                                    // regDate, propDesc, leaseDate
                                    regDate += " " + GetText(line, indexes.RegDateIndex, indexes.PropDesIndex);
                                    propDescription += " " + GetText(line, indexes.PropDesIndex, indexes.LeaseDateIndex);
                                    leaseDate += " " + GetText(line, indexes.LeaseDateIndex, length);
                                }
                                else if (length > indexes.LeaseTitleIndex)
                                {
                                    // regDate, propDesc, leaseDate, leaseTitle
                                    regDate += " " + GetText(line, indexes.RegDateIndex, indexes.PropDesIndex);
                                    propDescription += " " + GetText(line, indexes.PropDesIndex, indexes.LeaseDateIndex);

                                    if (indexes.HasLeaseTitle)
                                    {
                                        leaseDate += " " + GetText(line, indexes.LeaseDateIndex, indexes.LeaseTitleIndex);
                                        leaseTitle += " " + GetText(line, indexes.LeaseTitleIndex, length);
                                    }
                                    else
                                    {
                                        leaseDate += " " + GetText(line, indexes.LeaseDateIndex, length);
                                    }
                                }
                            }
                        }
                    }
                   
                }

                entry.RegistrationDatePlan = regDate;
                entry.PropertyDescription = propDescription;
                entry.LeaseDate = leaseDate;
                entry.LeaseTitle = leaseTitle;
                entry.Notes = notes;
                
                return entry;
            }

            return entry;
        }

        private Indexes GetIndexes(string str)
        {
            var regDateStartIndex = -1;
            var regDateEndIndex = -1;
            var propDescriptionIndex = -1;
            var leaseDateIndex = -1;
            var leaseTitleIndex = -1;
            var propDescriptionIsSet = false;
            var leaseTitleIsSet = false;

            string pattern = "[0-9]{2}.[0-9]{2}.[0-9]{4}";

            var matches = Regex.Matches(str, pattern);
            
            if (matches.Count() == 2)
            {
                regDateStartIndex = matches[0].Index;
                regDateEndIndex = regDateStartIndex + matches[0].Length;
                leaseDateIndex = matches[1].Index;                
            }

            if (matches.Count() == 1)
            {
                if (matches[0].Index > 0)
                {
                    leaseDateIndex = matches[0].Index;                    
                }
                else
                {
                    regDateStartIndex = matches[0].Index;
                    regDateEndIndex = regDateStartIndex + matches[0].Length;
                }
            }

            for (int i = Math.Max(regDateEndIndex, 0); i < str.Length; i++)
            {
                if (!char.IsWhiteSpace(str[i]) &&  i >= regDateStartIndex && !propDescriptionIsSet)
                {
                    propDescriptionIndex = i;
                    propDescriptionIsSet = true;
                    continue;
                }

                if (!char.IsWhiteSpace(str[i]) && char.IsUpper(str[i]) && i > leaseDateIndex && propDescriptionIsSet && !leaseTitleIsSet)
                {
                    leaseTitleIndex = i;
                    leaseTitleIsSet = true;
                    break;
                }
            }


            return new Indexes()
            {
                RegDateIndex = regDateStartIndex,
                PropDesIndex = propDescriptionIndex,
                LeaseDateIndex = leaseDateIndex,
                LeaseTitleIndex = leaseTitleIndex
            };
        }

        private string GetText(string str, int start, int end)
        {
            if (start < 0 || end < 0 || end < start)
                return "";

            if (end > str.Length)
            {
                end = str.Length;
            }

            if (start > str.Length)
            {
                start = 0; 
            }

            var result = str.Substring(start, end - start).Trim();
            return result;
        }
    }
}
