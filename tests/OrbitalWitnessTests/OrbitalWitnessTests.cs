using NUnit.Framework;
using OrbitalWitness.Challenge;
using System.Collections.Generic;

namespace OrbitalWitnessTests
{
    public class OrbitalWitnessTests
    {
        private Parser _SUT = new Parser();
        
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ItReturnsExpectedTexts1()
        {
            var line1 = "28.01.2009      Transformer Chamber (Ground   23.01.2009      EGL551039  ";
            var line2 = "tinted blue     Floor)                        99 years from              ";
            var line3 = "(part of)                                     23.1.2009";

            var texts = new List<string>() { line1, line2, line3 };

            var result = _SUT.Parse(texts);

            Assert.That(result.RegistrationDatePlan.Contains("28.01.2009 tinted blue (part of)"));
            Assert.That(result.PropertyDescription.Contains("Transformer Chamber (Ground Floor)"));
            Assert.That(result.LeaseDate.Contains("23.01.2009 99 years from 23.1.2009"));
            Assert.That(result.LeaseTitle.Contains("EGL551039"));
        }

        [Test]
        public void ItReturnsExpectedTexts2()
        {
            var line1 = "28.01.2009      Endeavour House, 47 Cuba   23.01.2009      EGL551039  ";
            var line2 = "Edged and       Street, London             125 years from              ";
            var line3 = "numbered 2 in                              1.1.2009";
            var line4 = "blue (part of)";

            var texts = new List<string>() { line1, line2, line3, line4 };

            var result = _SUT.Parse(texts);

            Assert.That(result.RegistrationDatePlan.Contains("28.01.2009 Edged and numbered 2 in blue (part of)"));
            Assert.That(result.PropertyDescription.Contains("Endeavour House, 47 Cuba Street, London"));
            Assert.That(result.LeaseDate.Contains("23.01.2009 125 years from 1.1.2009"));
            Assert.That(result.LeaseTitle.Contains("EGL551039"));
        }

        [Test]
        public void ItReturnsExpectedTexts_WithNote()
        {
            var line1 = "21.09.2015      Unit 4, Metro Triangle        17.09.2015      MM58541    ";
            var line2 = "Edged and                                     from and                   ";
            var line3 = "numbered 4 in                                 including                  ";
            var line4 = "blue (NSE)                                    29/09/2015 to              ";
            var line5 = "and including              ";
            var line6 = "28/09/2025                 ";
            var line7 = "NOTE: The lease grants right to use car parking spaces for edged and numbered 5 in blue on title plan.";

            var texts = new List<string>() { line1, line2, line3, line4, line5, line6, line7 };

            var result = _SUT.Parse(texts);

            Assert.That(result.RegistrationDatePlan.Contains("21.09.2015 Edged and numbered 4 in blue (NSE) and including 28/09/2025"));
            Assert.That(result.PropertyDescription.Contains("Unit 4, Metro Triangle"));
            Assert.That(result.LeaseDate.Contains("17.09.2015 from and including 29/09/2015 to"));
            Assert.That(result.LeaseTitle.Contains("MM58541"));
            Assert.That(result.Notes[0].Contains("NOTE: The lease grants right to use car parking spaces for edged and numbered 5 in blue on title plan."));
        }

        [Test]
        public void ItReturnsExpectedTexts_WithNotes()
        {
            var line1 = "21.09.2015      Unit 4, Metro Triangle        17.09.2015      MM58541    ";
            var line2 = "Edged and                                     from and                   ";
            var line3 = "numbered 4 in                                 including                  ";
            var line4 = "blue (NSE)                                    29/09/2015 to              ";
            var line5 = "and including              ";
            var line6 = "28/09/2025                 ";
            var line7 = "NOTE 1: The lease grants right to use car parking spaces for edged and numbered 5 in blue on title plan.";
            var line8 = "NOTE 2: The lease grants right to use car parking spaces for edged and numbered 5 in blue on title plan.";

            var texts = new List<string>() { line1, line2, line3, line4, line5, line6, line7, line8 };

            var result = _SUT.Parse(texts);

            Assert.That(result.RegistrationDatePlan.Contains("21.09.2015 Edged and numbered 4 in blue (NSE) and including 28/09/2025"));
            Assert.That(result.PropertyDescription.Contains("Unit 4, Metro Triangle"));
            Assert.That(result.LeaseDate.Contains("17.09.2015 from and including 29/09/2015 to"));
            Assert.That(result.LeaseTitle.Contains("MM58541"));
            Assert.That(result.Notes[0].Contains("NOTE 1: The lease grants right to use car parking spaces for edged and numbered 5 in blue on title plan."));
            Assert.That(result.Notes[1].Contains("NOTE 2: The lease grants right to use car parking spaces for edged and numbered 5 in blue on title plan."));
        }

        [Test]
        public void ItReturns1LineTextAsNote()
        {
            var line1 = "ITEM CANCELLED on 10 June 2015.";

            var texts = new List<string>() { line1 };

            var result = _SUT.Parse(texts);

            Assert.That(string.IsNullOrEmpty(result.RegistrationDatePlan));
            Assert.That(string.IsNullOrEmpty(result.PropertyDescription));
            Assert.That(string.IsNullOrEmpty(result.LeaseTitle));
            Assert.That(string.IsNullOrEmpty(result.LeaseDate));
            Assert.That(result.Notes[0].Contains("ITEM CANCELLED on 10 June 2015."));
        }

        [Test]
        public void ItReturnsExpectedTexts_WithNoTitle()
        {
            var line1 = "28.01.2009      Transformer Chamber (Ground   23.01.2009       ";
            var line2 = "tinted blue     Floor)                        99 years from              ";
            var line3 = "(part of)                                     23.1.2009";

            var texts = new List<string>() { line1, line2, line3 };

            var result = _SUT.Parse(texts);

            Assert.That(result.RegistrationDatePlan.Contains("28.01.2009 tinted blue (part of)"));
            Assert.That(result.PropertyDescription.Contains("Transformer Chamber (Ground Floor)"));
            Assert.That(result.LeaseDate.Contains("23.01.2009 99 years from 23.1.2009"));
            Assert.That(string.IsNullOrEmpty(result.LeaseTitle));
        }

        [Test]
        public void ItReturnsExpectedTexts_WithNoRegistrationDateAndRef()
        {
            var line1 = "Transformer Chamber (Ground   23.01.2009       MM58541";
            var line2 = "                              99 years from           ";
            var line3 = "                              23.1.2009";

            var texts = new List<string>() { line1, line2, line3 };

            var result = _SUT.Parse(texts);

            Assert.That(result.PropertyDescription.Contains("Transformer Chamber (Ground"));
            Assert.That(result.LeaseDate.Contains("23.01.2009 99 years from 23.1.2009"));
            Assert.That(result.LeaseTitle.Contains("MM58541"));
            Assert.That(string.IsNullOrWhiteSpace(result.RegistrationDatePlan));
        }
    }
}